variable "region" {
  type = string
  description = "Default region"
  default = "sa-east-1"
}

variable "runtime" {
  type = string
  description = "Lambda runtime"
  default = "nodejs14.x"
}

variable "lambda-env-environment-key" {
  type = string
  description = "Api Key to identify environment"
}
variable "environment" {
  type = string
  description = "Environment name for NODE_ENV"
  default = "development"
}

variable "environment-shortened" {
  type = string
  description = "Short Environment Name for function name"
  default = "dev"
}

variable "source-code-zip-filename" {
  type = string
  description = "ZIP Filename with source code"
  default = "lambda-code.zip"
}

variable "service-name" {
  type = string
  description = "Base Name for lambda function"
  default = "example-graphql"
}
variable "service-description" {
  type = string
  description = "Description for lambda function"
  default = "Exemplo de GraphQL"
}
variable "lambda-handler" {
  type = string
  description = "Lambda handler pointer"
  default = "dist/handler.default"
}

variable "lambda-timeout" {
  type = number
  default = 5
}

variable "lambda-memory-size" {
  type = number
  default = 128
}

variable "api-prefix" {
  type        = string
  default = "example-graphql"
}