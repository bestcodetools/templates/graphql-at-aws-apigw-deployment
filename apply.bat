@ECHO OFF & CLS

call set-credentials.bat

echo ACCOUNT_ID: %ACCOUNT_ID%
:selectEnv

echo 1 - Production
echo 2 - Homolog
echo 0 - to cancel
set /p selectedEnvId=Select an environment to deploy: 

if [%selectedEnvId%] == [0] goto canceled
if [%selectedEnvId%] == [1] goto setEnvProduction
if [%selectedEnvId%] == [2] goto setEnvHomolog

echo ------------------
echo Invalid selection
echo ------------------

echo Please inform a valid selection:
goto :selectEnv

:setEnvProduction

call set-prd-env-vars.bat

goto end


:setEnvHomolog

call set-hml-env-vars.bat

goto confirm
:confirm
set state_file=%ENVIRONMENT%.tfstate
echo --- Terraform Resource Variables ---
echo ENVIRONMENT                    = %ENVIRONMENT%
echo ENVIRONMENT_SHORTENED          = %ENVIRONMENT_SHORTENED%
echo --- Environemnt Variables        ---
echo NODE_ENV                       = %ENVIRONMENT%
echo ENVIRONMENT_KEY                = %LAMBDA_ENV_ENVIRONMENT_KEY%
echo --- Lambda Settings              ---
echo LAMBDA_TIMEOUT                 = %LAMBDA_TIMEOUT%
echo LAMBDA_MEMORY_SIZE             = %LAMBDA_MEMORY_SIZE%
echo --- Api Gateway Settings         ---
echo API_PREFIX                     = %API_PREFIX%
echo --- Terraform State Settings     ---
echo State File                     = %state_file%
echo ------------------------------------
set /p confirm=Type [y]es to confirm or [n]o to cancel: 
if [%confirm%] == [y] goto apply
if [%confirm%] == [yes] goto apply
if [%confirm%] == [n] goto canceled
if [%confirm%] == [no] goto canceled

goto confirm
:apply
set env_vars=
set env_vars=%env_vars% -var=runtime=nodejs14.x
set env_vars=%env_vars% -var=lambda-env-environment-key=%LAMBDA_ENV_ENVIRONMENT_KEY%
set env_vars=%env_vars% -var=environment=%ENVIRONMENT%
set env_vars=%env_vars% -var=environment-shortened=%ENVIRONMENT_SHORTENED%
set env_vars=%env_vars% -var=lambda-timeout=%LAMBDA_TIMEOUT%
set env_vars=%env_vars% -var=lambda-memory-size=%LAMBDA_MEMORY_SIZE%
set env_vars=%env_vars% -var=api-prefix=%API_PREFIX%
set env_vars=%env_vars% -var=aws_route53_zone-graphql-api-zone-name=%GRAPHQL_API_ZONE_NAME%
set env_vars=%env_vars% -var=aws_route53_zone-graphql-api-zone-id=%GRAPHQL_API_ZONE_ID%

set action=apply
if [%1] == [destroy] set action=destroy

set args=%action% -backup=%state_file%.backup -state=%state_file% %env_vars%
terraform %args%
if [%ERROR_LEVEL%] <> [0] goto end

echo Uploading terraform state...
set TERRAFORM_STATE_BUCKET_NAME=douglas-domingues-backups
aws s3 cp .\%state_file% s3://%TERRAFORM_STATE_BUCKET_NAME%/terraform/%API_PREFIX%/%state_file%
if exist "%state_file%.backup" aws s3 cp .\%state_file%.backup s3://%TERRAFORM_STATE_BUCKET_NAME%/terraform/%API_PREFIX%/%state_file%.backup

:try
curl --location --request POST https://%API_PREFIX%.%GRAPHQL_API_ZONE_NAME%/graphql/liveness > liveness.txt
set /p api_status=<liveness.txt 
del liveness.txt
if [%api_status%] == [OK] goto apiok
echo API Not available yet, retrying in 5 seconds...
ping 127.0.0.1 -n 5 < nul

goto try
:apiok
echo https://%API_PREFIX%.%GRAPHQL_API_ZONE_NAME%/graphql is now live!

goto end

:canceled
echo Canceled!
goto end

:end