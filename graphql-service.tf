resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_${var.service-name}-lambda-${var.environment-shortened}"

  assume_role_policy = "${file("./policies/lambda-role.json")}"
  tags = {
    "Service" = var.service-name
    "Environment" = var.environment
    "ShortEnvironment" = var.environment-shortened
  }
}

resource "aws_lambda_function" "graphql" {
  filename      = var.source-code-zip-filename
  function_name = "${var.service-name}-lambda-${var.environment-shortened}"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = var.lambda-handler
  timeout       = var.lambda-timeout
  memory_size   = var.lambda-memory-size

  source_code_hash = filebase64sha256(var.source-code-zip-filename)

  runtime = var.runtime
  tags = {
    "Service" = var.service-name
    "Environment" = var.environment
    "ShortEnvironment" = var.environment-shortened
  }

  environment {
    variables = {
      NODE_ENV = var.environment
      ENVIRONMENT_KEY= var.lambda-env-environment-key
    }
  }
}

resource "aws_api_gateway_rest_api" "graphql" {
  name        = "${var.service-name}-apigw-${var.environment-shortened}"
  description = var.service-description
  tags = {
    "Service" = var.service-name
    "Environment" = var.environment
    "ShortEnvironment" = var.environment-shortened
  }
}

resource "aws_api_gateway_resource" "api-proxy" {
  rest_api_id = aws_api_gateway_rest_api.graphql.id
  parent_id   = aws_api_gateway_rest_api.graphql.root_resource_id
  path_part   = "{proxy+}"
}

resource "aws_api_gateway_method" "api-method" {
  rest_api_id   = aws_api_gateway_rest_api.graphql.id
  resource_id   = aws_api_gateway_resource.api-proxy.id
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "api-lambda-integration" {
  rest_api_id          = aws_api_gateway_rest_api.graphql.id
  resource_id          = aws_api_gateway_resource.api-proxy.id
  http_method          = aws_api_gateway_method.api-method.http_method
  uri                  = aws_lambda_function.graphql.invoke_arn
  type                 = "AWS_PROXY"
  integration_http_method = "POST"
  timeout_milliseconds = 29000
}

# Allow Lambda to be activated from API Gateway

resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.graphql.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "${aws_api_gateway_rest_api.graphql.execution_arn}/*/*/*"
}

# API Gateway Deployment
resource "aws_api_gateway_deployment" "api" {
  rest_api_id = aws_api_gateway_rest_api.graphql.id
  stage_name = "production"

  triggers = {
    redeployment = sha1(jsonencode(aws_api_gateway_rest_api.graphql.body))
  }

  lifecycle {
    create_before_destroy = true
  }

  depends_on = [
    aws_api_gateway_method.api-method
  ]
}

resource "aws_api_gateway_stage" "api" {
  deployment_id = aws_api_gateway_deployment.api.id
  rest_api_id   = aws_api_gateway_rest_api.graphql.id
  stage_name    = "api"
}

# 

resource "aws_route53_record" "cert-record" {
  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 300
  type            = each.value.type
  zone_id         = var.aws_route53_zone-graphql-api-zone-id
}

resource "aws_acm_certificate_validation" "domains-cert-validation" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [for record in aws_route53_record.cert-record : record.fqdn]
}

# Add alias to API Gateway Lambda Integration to be used like: https://my-dns.com/{proxy+}

resource "aws_api_gateway_domain_name" "api" {
  domain_name              = "${var.api-prefix}.${data.aws_route53_zone.graphql-api-zone.name}"
  regional_certificate_arn = aws_acm_certificate_validation.domains-cert-validation.certificate_arn


  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_route53_record" "api" {
  zone_id = var.aws_route53_zone-graphql-api-zone-id
  name    = "${var.api-prefix}.${data.aws_route53_zone.graphql-api-zone.name}"
  type    = "A"
  alias {
    evaluate_target_health = true
    name                   = aws_api_gateway_domain_name.api.regional_domain_name
    zone_id                = aws_api_gateway_domain_name.api.regional_zone_id
  }
}

resource "aws_api_gateway_base_path_mapping" "api-mapping" {
  api_id      = aws_api_gateway_rest_api.graphql.id
  stage_name  = aws_api_gateway_stage.api.stage_name
  domain_name = aws_api_gateway_domain_name.api.domain_name
}

output "apigw-direct-invoke-url" {
  description = "apigw direct invoke url"
  value       = aws_api_gateway_stage.api.invoke_url
}