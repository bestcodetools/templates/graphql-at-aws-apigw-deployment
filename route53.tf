data "aws_route53_zone" "graphql-api-zone" {
  name = var.aws_route53_zone-graphql-api-zone-name
}
variable "aws_route53_zone-graphql-api-zone-name" {
  default = "yourdomain.com"
}
variable "aws_route53_zone-graphql-api-zone-id" {
  default = "ZXXXXXXXXXXXXXXXXXXXX"
}

