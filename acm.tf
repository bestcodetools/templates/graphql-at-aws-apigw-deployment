resource "aws_acm_certificate" "cert" {
  domain_name       = data.aws_route53_zone.graphql-api-zone.name
  validation_method = "DNS"

  tags = {
    Environment = var.environment
  }
  subject_alternative_names = [
    "${var.api-prefix}.${data.aws_route53_zone.graphql-api-zone.name}"
  ]
  lifecycle {
    create_before_destroy = true
  }
}